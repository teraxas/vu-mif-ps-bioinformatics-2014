__author__ = 'Karolis'

from Bio import AlignIO

from investigator import Investigator


investigator = Investigator()

file_name1 = "res/downloaded_data.fasta"
file_name2 = "res/mafft_processed_data.fasta"
enterez_query = '"serum albumin"[Protein name] AND mammals[Organism]'

investigator.download_data(file_name1, enterez_query)

# apply MAFFT to downloaded files and save
investigator.do_mafft(file_name1, file_name2)

# mafft result
alignment = AlignIO.read(file_name2, "fasta")

human = None
other_orgs = []

# sepparate humans/other organisms records
for record in alignment:
    if "ALBU_HUMAN" in record.id:
        human = record
    else:
        other_orgs.append(record)

human_len = len(human)

# panasiausia seka i zmogaus
solution1 = investigator.most_similar(human, other_orgs, human_len)

# fragmentas panasiausias i zmogaus
investigator.most_similar_fragment(human, other_orgs, human_len, 15)

# unikaliausias fragmentas
investigator.most_unique_fragment(human, other_orgs, human_len, 15)