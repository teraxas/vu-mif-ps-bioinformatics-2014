__author__ = 'Karolis'

import Bio.Blast.NCBIWWW
import Bio.Blast.NCBIXML
import os


class Investigator:
    def __init__(self):
        pass

    @staticmethod
    def execute_download(file_name, entrez_query):
        ncbi = Bio.Blast.NCBIWWW.qblast(program="blastp", database="swissprot",
                                        sequence="4502027", entrez_query=entrez_query,
                                        hitlist_size=500, expect=100.0)

        blast = Bio.Blast.NCBIXML.read(ncbi)
        file = open(file_name, "w")

        for sequence in blast.alignments:
            file.write('>%s\n' % sequence.title)
            file.write('%s\n' % sequence.hsps[0].sbjct)
        file.close()

    # Jei yra jau toks failas tuomet per nauja nera siunciama informacija
    def download_data(self, file_name, enterez_query):
        if os.path.exists(file_name):
            print "File already exists."
        else:
            print "Downloading..."
            self.execute_download(file_name, enterez_query)
            print "Done."

    @staticmethod
    def do_mafft(file_name1, file_name2):
        print "Starting MAFFT..."
        os.system("mafft --quiet " + file_name1 + " >" + file_name2)
        print "MAFFT completed."

    #panasiausia seka
    @staticmethod
    def most_similar(source, comparable, len):
        prev_count = 0
        count = 0
        most_similar = None
        for record in comparable:
            for x in range(0, len):
                if record.seq[x] == source.seq[x]:
                    count += 1
            if count >= prev_count:
                most_similar = record
                prev_count = count
            count = 0

        print ("Panasiausia seka i zmogaus yra:")
        print (most_similar.id)
        print (most_similar.seq)

    #panasiausias fragmentas
    @staticmethod
    def most_similar_fragment(source, comparable, len, frag_len):
        count = 0
        prev_count = 0
        fr = 0
        to = 0
        for x in range(0, len - frag_len):
            for record in comparable:
                for y in range(x, x + frag_len):
                    if record.seq[y] == source.seq[y]:
                        count += 1
            if count > prev_count:
                fr = x
                to = x + frag_len
                prev_count = count
                count = 0
            else:
                count = 0
        print ("Panasiausias fragmentas: nuo %d iki %d. Panasumai: %d" % (fr, to, prev_count))
        print ("Fragmentas: ")
        print (source.seq[fr:to])
        for record in comparable:
            print record.seq[fr:to]

    #unikaliausias fragmentas
    @staticmethod
    def most_unique_fragment(source, comparable, len, frag_len):
        unique_rows_count = 0
        all_diffs = 0
        tmp_diffs = 0
        prev_unique_rows = 0
        prev_all_differences = 0
        fr = 0
        to = 0
        for x in range(0, len - frag_len):
            for record in comparable:
                for y in range(x, x + frag_len):
                    # count diffs
                    if record.seq[y] != source.seq[y]:
                        tmp_diffs += 1
                if tmp_diffs != 0:
                    unique_rows_count += 1
                all_diffs += tmp_diffs
                tmp_diffs = 0
            if unique_rows_count >= prev_unique_rows:
                if unique_rows_count == prev_unique_rows:
                    if all_diffs > prev_all_differences:
                        prev_unique_rows = unique_rows_count
                        prev_all_differences = all_diffs
                        fr = x
                        to = x + frag_len
                else:
                    prev_unique_rows = unique_rows_count
                    prev_all_differences = all_diffs
                    fr = x
                    to = x + frag_len
            all_diffs = 0
            unique_rows_count = 0
        print ("Unikaliausias fragmentas: nuo %d iki %d. Skirtumai: %d" % (fr, to, prev_all_differences))
        print ("Fragmentas: ")
        print source.seq[fr:to]
        for record in comparable:
            print record.seq[fr:to]